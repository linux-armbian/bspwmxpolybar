#!/bin/bash

sudo apt install -y bspwm dmenu sxhkd picom numlockx rofi dunst libnotify-bin unzip scrot
sudo apt install -y xorg xbacklight xbindkeys xvkbd xinput dialog mtools dosfstools avahi-daemon acpid gvfs-backends xfce4-power-manager pcmanfm xfce4-terminal lxappearance feh usb-modeswitch

mkdir -p ~/.config/{bspwm,sxhkd,dunst} ; sleep 1
cp /usr/share/doc/bspwm/examples/bspwmrc ~/.config/bspwm/bspwmrc ; sleep 1
chmod 755 ~/.config/bspwm/bspwmrc ; sleep 1

cp /usr/share/doc/bspwm/examples/sxhkdrc ~/.config/sxhkd/sxhkdrc ; sleep 1
chmod 644 ~/.config/sxhkd/sxhkdrc ; sleep 1


sudo apt install -y lightdm lightdm-gtk-greeter-settings ; sleep 1

sudo systemctl restart lightdm
sudo systemctl restart avahi-daemon
sudo systemctl restart acpid

sudo apt -y autoremove ; sleep 2

echo "Success install bspwm!!"

